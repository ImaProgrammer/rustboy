# Rustboy
## A Game Boy Emulator written in Rust

### What is Rustboy?

- A Emulator that is written in Rust

- Able to play any Game Boy from the Game Boy to the Game Boy Color

- Once Game Boy and Game Boy Color emulation has been done, Game Boy Advance to Game Boy Micro might be implemented.
