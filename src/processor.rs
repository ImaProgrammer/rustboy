use register;

use register::RegisterFlag::{C, N, H, Z};

pub struct Processor {
    reg: register::Register,
    halted: bool,
}

impl Processor {
    pub fn new() -> Processor {
        Ok(Processor {
            reg: register::Register::new()
        })
    }
}
