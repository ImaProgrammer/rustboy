pub struct Memory {
    // rml = Reserved Memory Locations
    pub rml_res00address: u8, // Restart $00 Address
    pub rml_res08address: u8, // Restart $08 Address
    pub rml_res10address: u8, // Restart $10 Address
    pub rml_res18address: u8, // Restart $18 Address
    pub rml_res20address: u8, // Restart $20 Address
    pub rml_res28address: u8, // Restart $28 Address
    pub rml_res30address: u8, // Restart $30 Address
    pub rml_res38address: u8, // Restart $38 Address
    pub rml_vbisa: u8, // Vertical Blank Interrupt Start Address
    pub rml_lcdcsisa: u8, // LCDC Status Interrupt Start Address
    pub rml_toisa: u8, // Timer Overflow Interrupt Start Address
    pub rml_stcisa: u8, // Serial Transfer Completion Interrupt Start Address
    pub rml_hlisa: u8, // High-to-Low of P10-P13 Interrupt Start Address
    pub rml_beginexec: (u16, u16), // Begin Code Execution point in a Cartridge
    pub rml_startnintendogfx: (u16, u16), // Scrolling Nintendo Graphics
    pub rml_titlegame: (u16, u16), // Title of the game in UPPER CASE ASCII
    pub rml_indicator: u16, // GB/SGB Indicator
    pub rml_carttype: u16, // Cartridge Type
    pub rml_romsize: u16, // ROM Size
    pub rml_ramsize: u16, // RAM Size
    pub rml_destcode: u16, // Destination Code
    pub rml_licensecode: u16, // License Code
    pub rml_maskromvernum: u16, // Mask ROM Version Number
    pub rml_complementcheck: u16, // Complement Check
    pub rml_checksum: (u16, u16), // Checksum (Higher byte first)
    // Memory of the Game Boy
    pub video_ram: (u16, u16), // Game Boy Graphics Procssor Random Access Memory
    pub working_ram: (u16, u16), // Game Boy Working Random Access Memory
    pub working_ram_shadow: (u16, u16), // Game Boy Working Random Access Memory Shadow
    pub sprite_information: (u16, u16), // Game Boy Graphics Processor Sprite Information Memory
    pub mapped_io: (u16, u16), // Memory-mapped I/O
    pub zeropage_ram: (u16, u16), // Zero-page Random Access Memory
}

impl Memory {
    pub fn new() -> Memory {
        Memory {
            rml_res00address: 0x0000, // Restart $00 Address
            rml_res08address: 0x0008, // Restart $08 Address
            rml_res10address: 0x0018, // Restart $10 Address
            rml_res18address: 0x0018, // Restart $18 Address
            rml_res20address: 0x0020, // Restart $20 Address
            rml_res28address: 0x0028, // Restart $28 Address
            rml_res30address: 0x0030, // Restart $30 Address
            rml_res38address: 0x0038, // Restart $38 Address
            rml_vbisa: 0x0040, // Vertical Blank Interrupt Start Address
            rml_lcdcsisa: 0x0048, // LCDC Status Interrupt Start Address
            rml_toisa: 0x0050, // Timer Overflow Interrupt Start Address
            rml_stcisa: 0x0058, // Serial Transfer Completion Interrupt Start Address
            rml_hlisa: 0x0060, // High-to-Low of P10-P13 Interrupt Start Address
            rml_beginexec: (0x0100, 0x0103), // Begin Code Execution point in a Cartridge
            rml_startnintendogfx: (0x0104, 0x0133), // Scrolling Nintendo Graphics
            rml_titlegame: (0x0134, 0x0142), // Title of the game in UPPER CASE ASCII
            rml_indicator: 0x0146, // GB/SGB Indicator
            rml_carttype: 0x0147, // Cartridge Type
            rml_romsize: 0x0148, // ROM Size
            rml_ramsize: 0x0149, // RAM Size
            rml_destcode: 0x014A, // Destination Code
            rml_licensecode: 0x014B, // License Code
            rml_maskromvernum: 0x014C, // Mask ROM Version Number
            rml_complementcheck: 0x014D, // Complement Check
            rml_checksum: (0x014E, 0x014F), // Checksum (Higher byte first)

            video_ram: (0x8000, 0x9FFF), // Game Boy Graphics Procssor Random Access Memory
            working_ram: (0xC000, 0xDFFF), // Game Boy Working Random Access Memory
            working_ram_shadow: (0xE000, 0xFDFF), // Game Boy Working Shadow Random Access Memory
            sprite_information: (0xFE00, 0xFE9F), // Game Boy Graphics Processor Sprite Information Memory
            mapped_io: (0xFF00, 0xFF7F), // Memory-mapped I/O
            zeropage_ram: (0xFF80, 0xFFFF), // Zero-page Random Access Memory
        }
    }
}
