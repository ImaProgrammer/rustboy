#[derive(Copy, Clone)]
pub struct Register {
    // reg = Register
    pub a: u8, // Register A
    pub b: u8, // Register B
    pub c: u8, // Register C
    pub d: u8, // Register D
    pub e: u8, // Register E
    pub f: u8, // Register F
    pub h: u8, // Register H
    pub l: u8, // Register L
    pub sp: u16, // Register SP
    pub pc: u16 // Register PC
}

#[derive(Copy, Clone)]
pub enum RegisterFlag {
    C = 0b00010000,
    H = 0b00100000,
    N = 0b01000000,
    Z = 0b10000000,
}

impl Register {
    pub fn new() -> Register {
        Register {
            a: 0x01, // Register A
            b: 0x00, // Register B
            c: 0x13, // Register C
            d: 0x00, // Register D
            e: 0xD8, // Register E
            f: 0xB0, // Register F
            h: 0x01, // Register H
            l: 0x4D, // Register L
            sp: 0x0100, // Register SP
            pc: 0xFFFE, // Register PC
        }
    }

    pub fn af(&self) -> u16 {
        ((self.a as u16) << 8) | ((self.f & 0xF0) as u16)
    }
}
